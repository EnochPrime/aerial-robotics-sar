# Aerial Robotics - SAR

A motion planning project for an aerial robot performing a search and rescue (SAR) mission. 

## Installation
1. Install and initialize ROS Melodic with required dependencies.
``` shell
$ sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu `lsb_release -sc` main" > /etc/apt/sources.list.d/ros-latest.list'
$ wget http://packages.ros.org/ros.key -O - | sudo apt-key add -
$ sudo apt-get update
$ sudo apt-get install ros-melodic-desktop-full ros-melodic-control-toolbox ros-melodic-geographic-msgs ros-melodic-mavlink ros-melodic-octomap-ros python-rosdep python-wstool python-catkin-tools protobuf-compiler libgoogle-glog-dev libgeographic-dev geographiclib-tools
$ pip install PySide2 numpy scipy matplotlib
$ sudo rosdep init
$ rosdep update
$ source /opt/ros/melodic/setup.bash
```
2. Create a ROS workspace.
``` shell
$ mkdir -p ~/catkin_ws/src
$ cd ~/catkin_ws/src
$ catkin_init_workspace
$ wstool init
$ git archive --remote=git@gitlab.com:jdm12989/aerial-robotics-sar.git HEAD sar.rosinstall | tar -x
$ wstool merge sar.rosinstall
$ wstool update
```

3. Build workspace.
``` shell
$ cd ~/catkin_ws
$ catkin build
```

4. *[Optional]* Add workspace setup to bashrc.
``` shell
$ echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
$ source ~/catkin_ws/devel/setup.bash
```
> **Note** This will load workspace setup automatically.

## Usage

### Forest World with Control Panel
``` shell
$ roslaunch aerial_robotics_sar sar.launch
```
> **Note** You first need to run `source ~/catkin_ws/devel/setup.bash` if not configured to run automatically.

### Path Planning
``` shell
$ roslaunch rotors_gazebo mav_hovering_example_with_vi_sensor.launch mav_name:=firefly world_name:=basic
$ rosrun aerial_robotics_sar sar_plannner
```
> **Note** You first need to run `source ~/catkin_ws/devel/setup.bash` if not configured to run automatically.