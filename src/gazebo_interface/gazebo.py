""" gazebo.py
Abstraction from ROS/Gazebo messages and service calls.
"""
import rospy
from gazebo_msgs.srv import GetModelProperties, GetModelState, GetPhysicsProperties, GetWorldProperties
from std_srvs.srv import Empty

__author__ = "James McCandlish"
__maintainer__ = "James McCandlish"
__email__ = "jdmccandlish@wpi.edu"
__status__ = "Development"

class TimeoutException(Exception):
    pass


class ServiceException(Exception):
    pass


def _callService(service, response, *args, **kwargs):
    proxy = rospy.ServiceProxy(service, response)
    timeout = None
    if 'timeout' in kwargs:
        timeout = kwargs['timeout']

    # wait for service to be available
    try:
        rospy.wait_for_service(service, timeout=timeout)
    except rospy.ROSException:
        rospy.loginfo('service {} timed out'.format(service))
        raise TimeoutException

    # call service and receive response
    try:
        return proxy(*args)
    except rospy.ServiceException as exc:
        rospy.logerr('service did not process request: {}'.format(exc))
        raise ServiceException


def getModelCount():
    """ Returns the number of models in the world
    :return: number of models
    """
    return len(getWorldProperties().model_names)


def getModelProperties(modelName):
    """Gets the model properties
    :param modelName: name of the model
    :return model properties
    """
    resp = _callService('/gazebo/get_model_properties', GetModelProperties, modelName)
    return resp if resp.success else None


def getModelState(modelName, referenceModel):
    """Returns the model state
    :param modelName: name of the model
    :param referenceModel: name of the reference model
    :return: model state
    """
    resp = _callService('/gazebo/get_model_state', GetModelState, modelName, referenceModel)
    return resp if resp.success else None


def setModelState():
    # TODO: implement this method
    raise Exception('method not implemented')


def getModels():
    """ Returns a list of models in the world
    :return: list of models
    """
    # create model objects from names
    from gazebo_interface.models import createModel
    models = []
    for mdl in getWorldProperties().model_names:
        models.append(createModel(mdl))
    return models


def getPhysicsProperties(timeout=None):
    """Gets the physics properties
    :return physics properties
    """
    resp = _callService('/gazebo/get_physics_properties', GetPhysicsProperties, timeout=timeout)
    return resp if resp.success else None


def getWorldProperties():
    """Gets the world properties
    :return world properties
    """
    resp = _callService('/gazebo/get_world_properties', GetWorldProperties)
    return resp if resp.success else None


def isPaused():
    """Returns true if physics are paused
    :return true if physics are paused, otherwise false
    """
    physicsProperties = getPhysicsProperties(timeout=3)
    assert physicsProperties is not None
    return physicsProperties.pause


def pausePhysics():
    """Pauses physics
    :return: true if physics paused, otherwise false
    """
    _callService('/gazebo/pause_physics', Empty, timeout=3)


def unpausePhysics():
    """Unpauses Physics
    :return: true if physics unpause, otherwise false"""
    _callService('/gazebo/unpause_physics', Empty, timeout=3)


def resetSimulation():
    """Resets the simulation
    :return: true if reset, otherwise false"""
    _callService('/gazebo/reset_simulation', Empty, timeout=3)


def resetWorld():
    """Resets the world
    :return: true if reset, otherwise false"""
    _callService('/gazebo/reset_world', Empty, timeout=3)
