""" models.py
Wrappers for Gazebo models.
"""
import rospy
from gazebo_interface import gazebo

__author__ = "James McCandlish"
__maintainer__ = "James McCandlish"
__email__ = "jdmccandlish@wpi.edu"
__status__ = "Development"

def createModel(modelName):
    rospy.logdebug('Creating model for {}'.format(modelName))
    properties = gazebo.getModelProperties(modelName)

    if properties.body_names[0] == PineTree.BODYNAME:
        return PineTree(modelName, properties)
    elif properties.body_names[0] == Person.BODYNAME:
        return Person(modelName, properties)
    else:
        rospy.logwarn('unhandled type {} for model {}'.format(
            properties.body_names[0], modelName
        ))


class GazeboModel(object):
    BODYNAME = ''

    def __init__(self, modelName, properties):
        self.modelName = modelName
        self.isStatic = properties.is_static
        self.state = gazebo.getModelState(self.modelName, self.BODYNAME)

    @property
    def position(self):
        """Return the x,y,z location of model
        :return: position object containing x,y,z values"""
        if not self.isStatic:
            self.state = gazebo.getModelState(self.modelName, self.BODYNAME)

        return self.state.pose.position

    @property
    def orientation(self):
        """Return the orientation of model as Quaternion
        :return orientation object container x,y,z,w values"""
        if not self.isStatic:
            self.state = gazebo.getModelState(self.modelName, self.BODYNAME)

        return self.state.pose.orientation


class PineTree(GazeboModel):
    BODYNAME = 'pine_tree::link'

    def __init__(self, modelName, properties):
        super(PineTree, self).__init__(modelName, properties)


class Person(GazeboModel):
    BODYNAME = 'person_standing::link'

    def __init__(self, modelName, properties):
        super(Person, self).__init__(modelName, properties)
