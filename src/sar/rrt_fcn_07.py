# -*- coding: utf-8 -*-
"""
Created on Sun Mar 29 17:17:13 2020

@author: niles
"""

import numpy as np
from numpy import array
from random import randint
import matplotlib.pyplot as plt
from scipy.spatial import distance
import random

# inc_vec = array([[0,0],[0,1],[1,0],[0,-1],[-1,0],[1,1],[1,-1],[-1,1],[-1,-1]],np.float)
n_grid = np.array([200,200],dtype='float')
n_grid_slice = np.array([5.,5.],dtype='float')
# obs_size = int(30)
# obs_opt = 1
  
u_max = float(5)


def random_sample(x_in,obs_list,N=1):
    x_rand_out = np.zeros((N,len(x_in)),dtype='float')
    for j in range(0,N):
        while True:
            #Note: Randint performs better than gaussian
            #x_rand = x_in+array([randint(0,n_grid_slice[0]),randint(0,n_grid_slice[1])],np.float)
            x_rand = x_in + np.concatenate((np.random.normal(0,n_grid_slice[0]/2, 1),
                                            np.random.normal(0,n_grid_slice[1]/2, 1)))
            # x_rand = x_in + array([random.uniform(0,n_grid_slice[i]) for i in range(len(x_in))])
            if collision_free_state(x_rand,obs_list):
                break
        x_rand_out[j]=x_rand
    return x_rand_out

   
def collision_free_state(x_in,obs_list):
    # Return true if no collision
    # Add collision check letter
    if x_in[0]>n_grid[0] or x_in[1]>n_grid[1] or x_in[0]<0 or x_in[1]<0:
        return False
    # Test no obstacle
    # return True
    return checkObstacle(x_in,obs_list)

def dist(x_1,x_2):
    # Distance function
    # Return Manhattan distance
    # return abs(x_2[0]-x_1[0])+abs(x_2[1]-x_1[1])
    #return distance.euclidean(x_1,x_2)
    return np.linalg.norm(x_1-x_2)

def nearest_neighb(Tree_in,x_in):
    indx_nearest = 0;
    dist_nearest = 1e9
    for i1 in range(len(Tree_in)):
        if Tree_in[i1]['i_parent']<0:  # Return if parent is -1
            break
        # Check if one can add node to this branch
        dist_i1 = dist(Tree_in[i1]['nd_st'],x_in)
        if dist_i1<dist_nearest:
            dist_nearest = dist_i1
            indx_nearest = i1
    return array((indx_nearest,dist_nearest),dtype=[('i_nearest','int'),('d_nearest','float')])
            
def inv_dyn_fcn(x_s,x_f,inc_vec,obs_list):
    # Inverse function to find control inputs given start and end state
    dist_s_f = dist(x_s,x_f)
    x_3 = np.zeros((len(inc_vec),len(x_s)),dtype='float')
    dist_3_f = np.zeros(len(inc_vec),dtype='float')
    u_3 = np.zeros(len(inc_vec),dtype='float')
    for i1 in range(len(inc_vec)):
        flag_coll_free, x_3[i1], u_3[i1] = collision_free_path(x_s,inc_vec[i1],dist_s_f,obs_list)
        dist_3_f[i1] = dist(x_3[i1],x_f)
    i_u_min = np.argmin(dist_3_f)
    x_f_n = x_3[i_u_min]
    u_mag = u_3[i_u_min]
    #print((i_u_min,u_mag,x_f_n))
    return np.array((i_u_min,u_mag,x_f_n),dtype=[('i_u','int'),('u_mag','float'),('x_f_n','float',2)])

    
def collision_free_path(x_in,u_in,u_mag,obs_list):
    collis_check = True
    n_pt = 20
    for i1 in range(0,n_pt): # Check 10 equally spaced points between 0 and u_mag
        u_1 = u_mag*(i1+1)/n_pt
        x_1 = x_in+u_in*u_1
        if not collision_free_state(x_1,obs_list):
            collis_check = False
            break
    return collis_check,x_1,u_1 # x_1 = collision free distance
        
# This function needs update
def tree_rewire(Tree_in,indx_x_p,x_goal,do_rewire,inc_vec,obs_list):
    x_p_elem = Tree_in[indx_x_p]
    x_p = Tree_in[indx_x_p]['nd_st']
    x_p_cost = Tree_in[indx_x_p]['nd_cost']
    x_root = Tree_in[0]['nd_st']
    print("In Rewire")
    for i1 in range(0,indx_x_p):
        x_old = Tree_in[i1]['nd_st']
        cost_old = Tree_in[i1]['nd_cost']
        new_node_info = inv_dyn_fcn(x_p,x_old,inc_vec,obs_list)
        x_new = new_node_info['x_f_n']
        cost_new = node_cost(x_p,x_new,x_goal,x_p_cost[0])
        if dist(x_new,x_old)<1e-3:
            if cost_new[0] < cost_old[0]: # Use cost to reach
                print("Rewire")
                if do_rewire:
                    Tree_in[i1]['i_parent'] = indx_x_p 
                    Tree_in[i1]['i_u'] = new_node_info['i_u']
                    Tree_in[i1]['u_mag'] = new_node_info['u_mag']
                    # Tree_in[i1,i_nd_st] = x_new
                    Tree_in[i1]['nd_cost'] = cost_new
    print("Out Rewire")
    return 0

def node_cost(x_parent,x_new,x_goal,x_parent_cost):
    # x_parent_cost = cost to reach parent
    cost_to_reach = dist(x_parent,x_new)+x_parent_cost
    cost_to_go = dist(x_new,x_goal)
    cost_total = cost_to_reach + cost_to_go
    return array([cost_to_reach,cost_to_go,cost_total],dtype='float')
    
def grow_tree(Tree_in,indx_tree_in,x_in,x_goal,inc_vec,obs_list):
    # Find Nearest neighbor    
    nbr_info = nearest_neighb(Tree_in,x_in)
    # set nearest neighbor as parent
    indx_x_parent = nbr_info['i_nearest']
    x_parent = Tree_in[indx_x_parent]['nd_st']
    x_parent_cost = Tree_in['nd_cost'][indx_x_parent,0]
    # Get new node
    new_node_info = inv_dyn_fcn(x_parent,x_in,inc_vec,obs_list)
    x_new = new_node_info['x_f_n']
    # Update Tree   
    Tree_in[indx_tree_in]['i_parent'] = indx_x_parent 
    Tree_in[indx_tree_in]['i_u'] = new_node_info['i_u']
    Tree_in[indx_tree_in]['u_mag'] = new_node_info['u_mag']
    Tree_in[indx_tree_in]['nd_st'] = x_new
    #print(Tree_in[indx_tree_in])
    Tree_in[indx_tree_in]['nd_cost'] = node_cost(x_parent,x_new,x_goal,x_parent_cost)
    #print(Tree_in[indx_tree_in]['nd_cost'])
    return x_new

def init_tree(n_max_tree_nodes,x_start,x_goal):
    Tree_in = np.zeros(n_max_tree_nodes,dtype=[('i_parent','int8'),('i_u','int'),('u_mag','float'),
                                 ('nd_st','2float'),('nd_cost','3float')])
    Tree_in['i_parent'] = -1
    Tree_in[0]['nd_st'] = x_start
    Tree_in[0]['i_parent'] = 0
    Tree_in[0]['nd_cost'] = node_cost(x_start,x_start,x_goal,0)
    
    return Tree_in

def min_cost_node(Tree_in):
	return Tree_in[min_cost_indx(Tree_in)] 
	
def min_cost_indx(Tree_in):
    return np.argmin(Tree_in['nd_cost'][:,1])

def min_cost_state(Tree_in):
    return Tree_in[min_cost_indx(Tree_in)]['nd_st']
	
def plot_tree(Tree_in,line_type = 'r-'):
    for i1 in range(0,len(Tree_in)):
        if Tree_in[i1]['i_parent']<0:  # Return if parent is -1
            break
        x_1 = Tree_in[i1]['nd_st'] #.tolist()
        x_1_parent = Tree_in[i1]['i_parent']
        x_2 = Tree_in[x_1_parent]['nd_st'] #.tolist()
        plt.plot([x_1[0],x_2[0]],[x_1[1],x_2[1]],line_type)
    
    #plt.show()

def connect_trees(Tree_in_1,Tree_in_2,indx_tree_in):
    con_tree = array([-1,-1],np.int)
    con_tot_cost = 1e9
    for i1 in range(0,indx_tree_in):
        for j1 in range(0,indx_tree_in):
            if dist(Tree_in_1[i1,i_nd_st],Tree_in_2[j1,i_nd_st]) == 0:
                con_tot_cost_1 = Tree_in_1[i1,i_nd_cost]+Tree_in_2[j1,i_nd_cost]
                if con_tot_cost_1 < con_tot_cost:
                    con_tree = array([i1,j1],np.int)
                    con_tot_cost = con_tot_cost_1 
    return con_tree

def trace_path(Tree_in,indx_in):
    if indx_in==0:
        return array([indx_in,indx_in],np.int)
    indx_path =array([],np.int)
    indx_path = np.append(indx_path,indx_in)
    indx_parent = indx_path[-1]
    while indx_parent > 0:
        indx_parent = Tree_in[indx_path[-1]]['i_parent']
        indx_path = np.append(indx_path,indx_parent)
    return np.flip(indx_path)

def trace_path_state(Tree_in,indx_in):
    indx_path = trace_path(Tree_in,indx_in)
    return Tree_in[indx_path]['nd_st']
    
    
#%% Obstabcles
def createObstacle(obs_opt):
    obs_size=float(30)
    if obs_opt == 1 :
        x_obs_list = np.array([90, 104, 51, 54, 37])*int(n_grid[0]/200)
        y_obs_list = np.array([46, 90, 120, 6, 62])*int(n_grid[1]/200)
    elif obs_opt == 2 :
        x_obs_list = np.array([90, 104, 60, 54, 37, 81, 0])*int(n_grid[0]/200)
        y_obs_list = np.array([46, 90, 120, 0, 62, 180, 62])*int(n_grid[1]/200)
    obs_size_x = np.zeros(x_obs_list.shape[0],dtype=float)
    obs_size_y = np.zeros(y_obs_list.shape[0],dtype=float)
    for i1 in range(0,np.size(x_obs_list)):
        obs_size_x[i1] = np.min([x_obs_list[i1]+obs_size,n_grid[0]])
        obs_size_y[i1] = np.min([y_obs_list[i1]+obs_size,n_grid[1]])
        
    obs_list = [x_obs_list,y_obs_list,obs_size_x,obs_size_y]
    return obs_list

def checkObstacle(x_in,obs_list):
    coll_free = True # Checking for Collision free path
    #[x_obs_list,y_obs_list] = createObstacle(obs_opt)
    # obs_list = createObstacle(obs_opt)
    x_obs_list = obs_list[0]
    y_obs_list = obs_list[1]
    obs_size_x = obs_list[2]
    obs_size_y = obs_list[3]
    for i1 in range(0,np.size(x_obs_list)):
        #obs_size_x = np.min([x_obs_list[i1]+obs_size,n_grid[0]])-x_obs_list[i1]
        #obs_size_y = np.min([y_obs_list[i1]+obs_size,n_grid[1]])-y_obs_list[i1]
        if x_in[0]>=x_obs_list[i1] and x_in[0]<=obs_size_x[i1] and x_in[1]>=y_obs_list[i1] and x_in[1]<=obs_size_y[i1]:
            coll_free = False
            break
    return coll_free
        
def plotObstacle(obs_list,line_type = 'k-'):
    x_obs_list = obs_list[0]
    y_obs_list = obs_list[1]
    obs_size_x = obs_list[2]
    obs_size_y = obs_list[3]
    for i1 in range(0,np.size(x_obs_list)):
        '''
        plt.plot([x_obs_list[i1],y_obs_list[i1]],[x_obs_list[i1],obs_size_y[i1]],line_type)
        plt.plot([x_obs_list[i1],obs_size_y[i1]],[obs_size_x[i1],obs_size_y[i1]],line_type)
        plt.plot([obs_size_x[i1],obs_size_y[i1]],[obs_size_x[i1],y_obs_list[i1]],line_type)
        plt.plot([obs_size_x[i1],y_obs_list[i1]],[x_obs_list[i1],y_obs_list[i1]],line_type)
        '''
        plt.plot([x_obs_list[i1],x_obs_list[i1]],[y_obs_list[i1],obs_size_y[i1]],line_type)
        plt.plot([x_obs_list[i1],obs_size_x[i1]],[obs_size_y[i1],obs_size_y[i1]],line_type)
        plt.plot([obs_size_x[i1],obs_size_x[i1]],[obs_size_y[i1],y_obs_list[i1]],line_type)
        plt.plot([obs_size_x[i1],x_obs_list[i1]],[y_obs_list[i1],y_obs_list[i1]],line_type)
        
    