#!/usr/bin/env python
import os

if os.name == 'posix':
    import rospy
    from std_msgs.msg import Int32
    from geometry_msgs.msg import PoseStamped
    from geometry_msgs.msg import Pose
    from std_msgs.msg import String
    import time
import matplotlib.pyplot as plt
import math
import numpy as np
from numpy import array
import scipy
from scipy.spatial import distance
from rrt_fcn_07 import random_sample,init_tree,grow_tree,plot_tree,tree_rewire
from rrt_fcn_07 import connect_trees,trace_path,min_cost_state
from rrt_fcn_07 import min_cost_indx,trace_path_state
from rrt_fcn_07 import createObstacle,plotObstacle

# Global Parameters
step_dist = 4 #0.3
goal_dist = 2.0 # Keep same as search grid n_grid_slice
n_max_tree_nodes = 50
ob_opt = 2
if os.name == 'posix':
    run_ros = True
else:
    run_ros = False
run_time_steps = 1000

# Globals: Path 
pos_cur = np.array([0,0],np.float)
pos_prev = np.array([0,0],np.float)
# pos_final = array([200,200],np.float)
# pos_final = array([175, 150],np.float)
pos_final = array([100, 170],np.float)
#pos_final = array([20, 125],np.float)

def euclideanDist(a, b):
    """ Calculates the Euclidean distance between two states.
    :param a: coordinates of point A
    :param b: coordinates of point B
    """
    # https://stackoverflow.com/questions/1401712/how-can-the-euclidean-distance-be-calculated-with-numpy
    return np.linalg.norm(a-b)

def move(a, b,stepDist=0.2):
    """ Simulates moving from A to B.
    :param a: point A
    :param b: point B
    :return: new point after moving a short distance toward B
    """
    v = b-a
    v_norm = np.linalg.norm(v)
    if v_norm>1e-6:
        return (a + stepDist * v / v_norm)
    else:
        return a

def control_vec(n_inc_vec):
    inc_vec = np.zeros((n_inc_vec+1,2),dtype=float)
    for i in range(0,n_inc_vec):
        inc_vec[i] = [np.sin(float(i)/n_inc_vec*2.*np.pi),np.cos(float(i)/n_inc_vec*2.*np.pi)]
    return inc_vec


def get_pos(data):
    global pos_cur, pos_prev
    pos_prev = pos_cur
    pos_cur = array([data.position.x, data.position.y],np.float)

def navigate(x_0,x_prev,local_wp,i_next_wp):
    # vel_est = x_0-x_prev
    vel_est = np.array([0,0],np.float)
    next_wp = local_wp[i_next_wp]
    wp_dist = euclideanDist(x_0,next_wp)
    if wp_dist < step_dist:
        if i_next_wp < len(local_wp)-1:
            i_next_wp+=1
            next_wp = local_wp[i_next_wp]
            next_wp = move(x_0,next_wp,step_dist)-vel_est
    else:
        next_wp = move(x_0,next_wp,step_dist)-vel_est
    return next_wp,i_next_wp

# Main to be designed for ROS and local execution
def talker():
#if __name__ == '__main__':
    local_wp=np.array([pos_cur,pos_final])
    indx_next_wp=1
    inc_vec = control_vec(8)
    if not run_ros:
        x_y_pos = np.zeros([run_time_steps,2],dtype=float)
        x_y_pos[0] = pos_cur
        pos_cmd = pos_cur
    else:
        rospy.init_node('pose_out', anonymous=True)
        pub = rospy.Publisher('/firefly/command/pose',PoseStamped, queue_size=10)    
        rate = rospy.Rate(10) # 10hz
        msg = PoseStamped()
        msg_pose = Pose()
        msg_pose.position.z = 2;
        rospy.Subscriber('/firefly/ground_truth/pose', Pose,get_pos)
        time.sleep(3)  # wait 3 sec to get initial data
        
    cntr = 0
    while not rospy.is_shutdown():
 #   for i1 in range(1,run_time_steps):
        cntr+=1
        if cntr%10==0:
            cntr = 0
            # 1Hz Loop
            x_start = pos_cur
            x_goal = pos_final
            obs_list = createObstacle(ob_opt)
            Tree_s = init_tree(n_max_tree_nodes,x_start,x_goal)
            x_rand = random_sample(x_start,obs_list,N=n_max_tree_nodes)
            goal_reached = distance.euclidean(x_start,x_goal) < goal_dist
            for indx_tree_s in range(1,n_max_tree_nodes):
                x_new = grow_tree(Tree_s,indx_tree_s,x_rand[indx_tree_s],x_goal,inc_vec,obs_list)
                #tree_rewire(Tree_s,indx_tree_s,x_goal,True,inc_vec,obs_list)
          
            x_min_indx = min_cost_indx(Tree_s)
            x_path = trace_path_state(Tree_s,x_min_indx)
            # local_wp = x_path
            # Use last waypoint for smoother path
            local_wp = np.array([pos_cur,x_path[-1]])
            indx_next_wp=1
            if not run_ros:
                plot_tree(Tree_s)
        # Execute for every step
        if not run_ros:
            get_pos(pos_cmd)
        else:
            rospy.Subscriber('/firefly/ground_truth/pose', Pose,get_pos)
            
        pos_cmd,indx_next_wp = navigate(pos_cur,pos_prev,local_wp,indx_next_wp)
        if not run_ros:
            x_y_pos[i1] = pos_cmd
        else:
            msg_pose.position.x = pos_cmd[0]
            msg_pose.position.y = pos_cmd[1]
            print(np.concatenate([pos_cmd,pos_cur]))
            msg.pose = msg_pose
            pub.publish(msg)
            rate.sleep()
    if not run_ros:           
        plt.plot(x_y_pos[:,0],x_y_pos[:,1])   
        obs_list = createObstacle(ob_opt)
        plotObstacle(obs_list,'m-')

'''
def talker():
    rospy.init_node('pose_out', anonymous=True)
    pub = rospy.Publisher('/firefly/command/pose',PoseStamped, queue_size=10)    
    rate = rospy.Rate(10) # 10hz
    msg = PoseStamped()
    msg_pose = Pose()
    msg_pose.position.z = 2;
    rospy.Subscriber('/firefly/ground_truth/pose', Pose,get_pos)
    time.sleep(3)  # wait 3 sec to get initial data
    while not rospy.is_shutdown():
        rospy.Subscriber('/firefly/ground_truth/pose', Pose,get_pos)
        #print(pos_cur)
        wp_n = navigate()
        msg_pose.position.x = wp_n[0]
        msg_pose.position.y = wp_n[1]
        print("cmd")
        print(wp_n)
        msg.pose = msg_pose
        pub.publish(msg)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass

if __name__ == '__main__':
    talker()
'''        