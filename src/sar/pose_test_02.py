#!/usr/bin/env python
import rospy
import time
from std_msgs.msg import Int32
from geometry_msgs.msg import PoseStamped
from geometry_msgs.msg import Pose
from std_msgs.msg import String
import matplotlib.pyplot as plt
import math
import numpy as np
from numpy import array
import scipy
from scipy.spatial import distance

max_local_wp = 10
# x,y and closest dist
# Example Waypoints
local_wp = np.zeros([max_local_wp,3],dtype=float)
local_wp[:,2] = 1e6
local_wp[0:3,0:2] =np.array([[0,0],[5,0],[5,5]],np.float)
step_dist = 0.3
indx_next_wp = 0
indx_final_wp = 2
x_y_pos = np.zeros([500,2],dtype=float)
pos_cur = []
pos_prev = []

def navigate():
    # Interpolate waypoint data to generate wapoint for next time step
    global indx_next_wp, pos_cur, pos_prev
    x_0 = array([pos_cur.position.x,pos_cur.position.y],np.float)
    x_prev = array([pos_prev.position.x,pos_prev.position.y],np.float)
    vel_est = x_0-x_prev
    next_wp = local_wp[indx_next_wp,0:2]
    wp_dist = distance.euclidean(next_wp,x_0)
    if wp_dist<step_dist:
        if indx_next_wp < indx_final_wp:
            indx_next_wp+=1
            next_wp = local_wp[indx_next_wp,0:2]
            wp_dist = distance.euclidean(next_wp,x_0)
            next_wp = x_0-vel_est+step_dist*(next_wp-x_0)/wp_dist
    else:
        next_wp = x_0-vel_est+step_dist*(next_wp-x_0)/wp_dist
            
    return next_wp    

def get_pos(data):
    # Get Current position data from ROS 
    global pos_cur, pos_prev
    pos_prev = pos_cur
    pos_cur = data
    #print(data)

def talker():
    # Send position data to aircraft  
    rospy.init_node('pose_out', anonymous=True)
    pub = rospy.Publisher('/firefly/command/pose',PoseStamped, queue_size=10)    
    rate = rospy.Rate(10) # 10hz
    msg = PoseStamped()
    msg_pose = Pose()
    msg_pose.position.z = 2;
    rospy.Subscriber('/firefly/ground_truth/pose', Pose,get_pos)
    time.sleep(3)  # wait 3 sec to get initial data
    cntr = 0
    while not rospy.is_shutdown():
        ''' 
        t = rospy.Time.from_sec(time.time())
        t_s = t.to_sec() #floating point
        msg_pose.position.x = 5*math.sin(2*math.pi*0.1*t_s);
        '''
        rospy.Subscriber('/firefly/ground_truth/pose', Pose,get_pos)
        #print(pos_cur)
        wp_n = navigate()
        msg_pose.position.x = wp_n[0]
        msg_pose.position.y = wp_n[1]
        print("cmd")
        print(wp_n)
        msg.pose = msg_pose
        pub.publish(msg)
        if cntr%10==0:
            print('1 hz Loop')
            cntr = 0
        cntr+=1
        rate.sleep()
    
if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass