""" ui.py
A PySide2 user interface for Search and Rescue (SAR).
Provides control over Gazebo and requesting missions. Also plots location of UAV and all obstacles.
"""
import re
from collections import defaultdict
from functools import wraps, partial
from operator import attrgetter

import rospy
from PySide2.QtCore import Slot, Qt
from PySide2.QtWidgets import QAction, QLabel, QPushButton, QMainWindow, QMessageBox, QHBoxLayout, QVBoxLayout, QWidget
from gazebo_msgs.msg import ModelStates
from aerial_robotics_sar.srv import GetMission

from gazebo_interface import gazebo
from sar_ui.realtimeChart import RealTimeChart

__author__ = "James McCandlish"
__maintainer__ = "James McCandlish"
__email__ = "jdmccandlish@wpi.edu"
__status__ = "Development"

def ServiceRertyLoop(f):
    @wraps(f)
    def wrapper(caller, *args, **kwargs):
        retry = True
        while retry:
            try:
                # try wrapped function
                return f(caller, *args, **kwargs)
            except gazebo.TimeoutException:
                # prompt for response
                resp = QMessageBox.information(
                    caller,
                    'Retry',
                    'Service call timed out.',
                    QMessageBox.Retry | QMessageBox.Cancel,
                    QMessageBox.Retry
                )
                retry = resp == QMessageBox.Retry
            except gazebo.ServiceException:
                # prompt for response
                resp = QMessageBox.information(
                    caller,
                    'Retry',
                    'Service call failed.',
                    QMessageBox.Retry | QMessageBox.Cancel,
                    QMessageBox.Retry
                )
                retry = resp == QMessageBox.Retry

    return wrapper


# noinspection PyAttributeOutsideInit
class MainUi(QMainWindow):
    def __init__(self):
        rospy.init_node('listener', anonymous=True, log_level=rospy.DEBUG)
        QMainWindow.__init__(self)
        self.setWindowTitle('SAR Interface')

        # dictionary for collected data
        self.dataMap = defaultdict(list)

        # create the interface
        self.createUi()
        self.createActions()
        self.createMenus()
        self.statusBar()
        self.statusBar().showMessage('Ready')

        # start listening for data
        self.startListener()

    def createActions(self):
        # reset world
        self.actResetWorld = QAction('Quick Reset', self)
        self.actResetWorld.setStatusTip('Resets the world')
        self.actResetWorld.triggered.connect(self.reset)

        # reset simulation
        self.actResetSim = QAction('Restart', self)
        self.actResetSim.setStatusTip('Restarts the simulation')
        self.actResetSim.triggered.connect(self.restart)

        # pause physics
        self.actPausePhysics = QAction('Pause', self)
        self.actPausePhysics.setStatusTip('Pause Physics')
        self.actPausePhysics.triggered.connect(self.pause)

        # unpause physics
        self.actUnPausePhysics = QAction('UnPause', self)
        self.actUnPausePhysics.setStatusTip('UnPause Physics')
        self.actUnPausePhysics.triggered.connect(self.unpause)

    def createMenus(self):
        simMenu = self.menuBar().addMenu('&Simulation')
        simMenu.addAction(self.actPausePhysics)
        simMenu.addAction(self.actUnPausePhysics)
        simMenu.addAction(self.actResetWorld)
        simMenu.addAction(self.actResetSim)

    def createUi(self):
        # widgets
        self.mainWidget = QWidget()
        self.btGetAssignment = QPushButton('Get Assignment')
        self.btGetAssignment.clicked.connect(self.getMission)
        self.btAbort = QPushButton('Abort')
        self.btAbort.setEnabled(False)
        self.text = QLabel('Hello World')
        self.text.setAlignment(Qt.AlignCenter)

        # live data view
        self.liveChart = RealTimeChart()
        self.liveChart.setXRange(-100, 100)
        self.liveChart.setYRange(-100, 100)

        # add location of UAV
        self.liveChart.addScatterSeries('uav', name='UAV',
                                        markerColor=Qt.blue)
        self.dataMap['/gazebo/model_states'].append(
            DataMapping(('pose.position.x', 'pose.position.y'),
                        partial(self.liveChart.updateSeries, 'uav'),
                        filterBy='name',
                        filter='.*firefly.*')
        )

        # add tree locations
        self.liveChart.addScatterSeries('trees', name='Trees',
                                        markerSize=10.0,
                                        markerColor=Qt.darkGreen)
        self.dataMap['/gazebo/model_states'].append(
            DataMapping(('pose.position.x', 'pose.position.y'),
                        partial(self.liveChart.updateSeries, 'trees'),
                        filterBy='name',
                        filter='.*tree.*')
        )

        # # add goal location
        # self.liveChart.addScatterSeries('goal', name='Goal',
        #                                 markerColor=Qt.red)
        # self.dataMap['/aerial_robotics_sar/mission'].append(
        #     DataMapping(('location.x', 'location.y'),
        #                 partial(self.liveChart.updateSeries, 'goal'))
        # )

        # left side
        self.layoutLeft = QVBoxLayout()
        self.layoutLeft.addWidget(self.btGetAssignment)
        self.layoutLeft.addWidget(self.text)
        self.layoutLeft.addWidget(self.btAbort)

        # main layout
        self.layout = QHBoxLayout()
        self.layout.addLayout(self.layoutLeft)
        self.layout.addWidget(self.liveChart)
        self.mainWidget.setLayout(self.layout)
        self.setCentralWidget(self.mainWidget)

    @Slot()
    @ServiceRertyLoop
    def pause(self):
        gazebo.pausePhysics()

    @Slot()
    @ServiceRertyLoop
    def unpause(self):
        gazebo.unpausePhysics()

    @Slot()
    @ServiceRertyLoop
    def reset(self):
        gazebo.resetWorld()

    @Slot()
    @ServiceRertyLoop
    def restart(self):
        gazebo.resetSimulation()

    @Slot()
    def getMission(self):
        rospy.loginfo('requesting mission')
        resp = None
        try:
            # TODO: should gazebo._callService not be a "private" function?
            resp = gazebo._callService('/aerial_robotics_sar/get_mission', GetMission, timeout=3)
        except gazebo.TimeoutException:
            resp = QMessageBox.information(
                self,
                'Timeout',
                'Failed to receive mission.',
                QMessageBox.Ok,
                QMessageBox.Ok
            )
            return

        rospy.logdebug(resp)
        self.text.setText(
            'Mission Target:\n  ({:.1f},{:.1f},{:.1f})\nRadius:\n  {}'.format(
                resp.location.x, resp.location.y, resp.location.z, resp.radius
            )
        )

    def startListener(self):
        # rospy.Subscriber(
        #     '/firefly/ground_truth/pose', Pose,
        #     callback=self.dataReceived,
        #     callback_args=('/firefly/ground_truth/pose',
        #                    ['position.x', 'position.y', 'position.z'])
        # )

        rospy.logdebug('subscribing to /gazebo/model_states')
        rospy.Subscriber(
            '/gazebo/model_states', ModelStates,
            callback=self.dataReceived,
            callback_args=('/gazebo/model_states', []),
            queue_size=1
        )

        # rospy.logdebug('subscribing to /aerial_robotics_sar/mission')
        # rospy.Subscriber(
        #     '/aerial_robotics_sar/mission', None,
        #     callback=self.dataReceived,
        #     callback_args=('/aerial_robotics_sar/mission', [])
        # )

    def dataReceived(self, data, args):
        """Handles receiving data from ROS topic
        :param data: received data from topic
        :param args: topic and request attributes from data
        """
        topic = args[0]
        attrs = args[1]
        rospy.logdebug(topic)
        rospy.logdebug(attrs)

        # # loop through all attributes
        # for attr in attrs:
        #     k = '{}.{}'.format(topic, attr)
        #     rospy.logdebug('processing {}'.format(k))
        #     if k not in self.dataMap:
        #         continue
        #
        #     # call all partial functions
        #     for pfunc in self.dataMap[k]:
        #         # assert hasattr(data, attr)
        #         v = attrgetter(attr)(data)
        #         rospy.logdebug(v)
        #         pfunc(v)

        rospy.logdebug('checking mappings for {}'.format(topic))
        assert topic in self.dataMap
        mappings = self.dataMap[topic]

        # process mappings
        for m in mappings:
            # handle filterBy attribute
            if m.filterBy is not None:
                # loop through filterBy attribute
                rospy.logdebug('filtering by {}'.format(m.filterBy))
                assert hasattr(data, m.filterBy)
                assert m.filter is not None
                filterAttr = attrgetter(m.filterBy)(data)
                idxs = []
                rospy.logdebug('matching against {}'.format(m.filter))
                for i, v in enumerate(filterAttr):
                    if re.match(m.filter, v):
                        # add indices to list
                        idxs.append(i)

                # loop through attributes
                filtData = []
                for attr in m.attributes:
                    # get list of main attribute
                    attrs = attr.split('.')
                    l = [attrgetter(attrs[0])(data)[i] for i in idxs]

                    # get sub-attribute data
                    filtData.append([attrgetter('.'.join(attrs[1:]))(elem) for elem in l])
                # sample
                # filtData[x][y] => attribute x, filtered index y

                # reshape data
                outData = []
                for i in range(len(filtData[0])):
                    elem = []
                    for j in range(len(filtData)):
                        elem.append(filtData[j][i])
                    outData.append(elem)

                # run function
                rospy.logdebug('running partial function')
                m.pfunc(outData)
            else:
                # loop through attributes
                filtData = []
                for attr in m.attributes:
                    filtData.append(attrgetter(attr)(data))

                # reshape data
                outData = [filtData]

                # run function
                rospy.logdebug('running partial function')
                m.pfunc(outData)


class DataMapping(object):
    def __init__(self, attributes, pfunc, filterBy=None, filter=None):
        self.attributes = attributes
        self.pfunc = pfunc
        self.filterBy = filterBy
        self.filter = filter
