""" realtimeChart.py
Contains a RealTimeChart class which is a self-contained QChartView object.
Can handle be provided new data points and update dynamically.
"""
from PySide2.QtCharts import QtCharts
from PySide2.QtCore import QPointF, Qt

__author__ = "James McCandlish"
__maintainer__ = "James McCandlish"
__email__ = "jdmccandlish@wpi.edu"
__status__ = "Development"

class RealTimeChart(QtCharts.QChartView):
    def __init__(self):
        self.chart = QtCharts.QChart()
        QtCharts.QChartView.__init__(self, self.chart)

        self.series = {}
        self.axisX = QtCharts.QValueAxis()
        self.axisX.setRange(0, 2000)
        self.axisY = QtCharts.QValueAxis()
        self.axisY.setRange(0, 100)

    def setTitle(self, title):
        self.chart.setTitle(title)

    def setXLabel(self, label):
        self.axisX.setTitleText(label)

    def setXRange(self, xmin, xmax):
        self.axisX.setRange(xmin, xmax)

    def setYLabel(self, label):
        self.axisY.setTitleText(label)

    def setYRange(self, ymin, ymax):
        self.axisY.setRange(ymin, ymax)

    def showLegend(self):
        self.chart.legend().show()

    def hideLegend(self):
        self.chart.legend().hide()

    def addLineSeries(self, seriesId):
        """Add line series to chart
        :param seriesId: unique id of series
        """
        self.series[seriesId] = QtCharts.QLineSeries()
        self.chart.addSeries(self.series[seriesId])
        self.chart.setAxisX(self.axisX, self.series[seriesId])
        self.chart.setAxisY(self.axisY, self.series[seriesId])

    def addScatterSeries(self, seriesId, name='',
                         markerShape=QtCharts.QScatterSeries.MarkerShape.MarkerShapeCircle,
                         markerSize=15.0,
                         markerColor=Qt.black):
        """Add scatter series to chart
        :param seriesId: unique id of series
        :param name: name of the series
        :param markerShape: shape of the series markers
        :param markerSize: size of the series markers
        :param markerColor: color of the series markers
        """
        # create series
        assert seriesId not in self.series
        series = QtCharts.QScatterSeries()
        series.setName(name)
        series.setMarkerShape(markerShape)
        series.setMarkerSize(markerSize)
        series.setColor(markerColor)

        # add series to chart
        self.series[seriesId] = series
        self.chart.addSeries(self.series[seriesId])
        self.chart.setAxisX(self.axisX, self.series[seriesId])
        self.chart.setAxisY(self.axisY, self.series[seriesId])

    def updateSeries(self, seriesId, data):
        """Updates data in specified series
        :param seriesId: unique id of series
        :param data: new data to append
        """
        # remap old points to new array
        assert seriesId in self.series
        # oldPts = self.series[seriesId].pointsVector()
        newPts = []
        # if len(oldPts) < (self.axisX.max() - self.axisX.min()):
        #     newPts = oldPts
        # else:
        #     for i, j in enumerate(oldPts):
        #         if i == 0:
        #             continue
        #         newPts.append(QPointF(i, j.y()))
        #
        # # append new data
        # assert isinstance(data, float)
        # newPts.append(QPointF(len(oldPts), data))
        for elem in data:
            assert len(elem) == 2
            newPts.append(QPointF(*elem))

        # replace data in series
        self.series[seriesId].replace(newPts)

    def removeSeries(self, seriesId):
        # TODO: implement this method
        raise Exception('method not implemented')
